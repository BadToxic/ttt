using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class TicTacToeController : MonoBehaviour {

    [Tooltip("Player 1 is played by the AI")]
    [HideInInspector]
    public bool p1Ai = false;

    [Tooltip("Player 2 is played by the AI")]
    [HideInInspector]
    public bool p2Ai = true;

    [SerializeField]
    private List<GameObject> aiLoadingSpinners;

    [Tooltip("Using hard coded instructions for the first two moves to improve the speed")]
    [HideInInspector]
    public bool useShortcuts = true;

    [Tooltip("Cutting of evaluation paths that can not become better than previous tested ones")]
    [HideInInspector]
    public bool useAlphaBetaPruning = true;

    [Tooltip("Let the AI not explore further than the given depth")]
    // [HideInInspector]
    public int maxExploreDepth = 6;

    [Tooltip("Visualize the AI algorithm step by step")]
    [HideInInspector]
    public bool visualizeAI = false;

    [Tooltip("Duration of each AI algorithm step in seconds")]
    [HideInInspector]
    public float algorithmStepDuration = 1;

    [Tooltip("How many fields in a line are needed to win")]
    [HideInInspector]
    public int chainLengthToWin = 3;

    [SerializeField]
    private GameObject fieldRowPrefab;
    [SerializeField]
    private GameObject fieldButtonPrefab;

    [SerializeField]
    private Transform gameAreaPanel;

    [SerializeField]
    private List<Button> buttons = new List<Button>();
    private int fieldWidth = 3; // Per row
    private int fieldHeight = 3; // Per column

    public delegate void OnGameOverDelegate(int win);
    public OnGameOverDelegate onGameOverDelegate;

    private bool turn; // true: Player, false: AI
    private int fieldsLeft;
    private bool isGameOver = true;

    // Will hold the current values of the MinMax algorithm
    private int recursionScore;
    private int optimalScoreButtonIndex = -1;
    // To prevent freezes we make a little pause after every:
    private int endFrameAfterSteps = 2500;

    public void Start() {
        ExtractButtonLines();
    }
    public void StartGame() {
        turn = Mathf.Round(UnityEngine.Random.Range(0, 1)) == 1;
        ExtractButtonLines();
        Reset();
    }
    private void SetFieldDimension() {
        // Delete old buttons
        foreach (Transform row in gameAreaPanel) {
            Destroy(row.gameObject);
        }
        buttons.Clear();

        int fontSize = 228 - 20 * Math.Max(fieldWidth, fieldHeight);

        // Create new buttons
        for (int rowIndex = 0; rowIndex < fieldHeight; rowIndex++) {
            GameObject row = Instantiate(fieldRowPrefab, gameAreaPanel);
            row.name = "Row " + rowIndex + 1;

            for (int columnIndex = 0; columnIndex < fieldWidth; columnIndex++) {
                Button button = Instantiate(fieldButtonPrefab, row.transform).GetComponent<Button>();
                button.onClick.AddListener(() => OnButtonClick(button));
                buttons.Add(button);
                button.name = "Button " + buttons.Count;
                SetFontSize(button, fontSize);
            }
        }
        ExtractButtonLines();
        Reset();
    }
    public void SetFieldWidth(int width) {
        fieldWidth = width;
        SetFieldDimension();
    }
    public void SetFieldHeight(int height) {
        fieldHeight = height;
        SetFieldDimension();
    }
    public void setChainLength(int length) {
        chainLengthToWin = length;
        Reset();
    }
    public void AdaptMaxDepthDynamically() {
        maxExploreDepth = Math.Max(4, 13 - fieldsLeft / 3);
    }

    List<List<Button>> buttonLines = new List<List<Button>>();
    // Get rows, columns and diaognals for faster checking
    private void ExtractButtonLines() {
        buttonLines.Clear();

        // Debug.Log("ExtractButtonLines for " + fieldWidth + "x" + fieldHeight);
        string test1 = "";
        string test2 = "";
        string test3 = "";
        string test4 = "";

        // Diagonals
        for (int k = 0; k <= fieldWidth + fieldHeight - 2; k++) {
            List<Button> buttonLine1 = new List<Button>();
            List<Button> buttonLine2 = new List<Button>();
            for (int j = 0; j <= k; j++) {
                int i = k - j;
                if (i < fieldHeight && j < fieldWidth) {
                    test1 += buttons[i * fieldWidth + j].name;
                    test2 += buttons[i * fieldWidth + (fieldWidth - 1 - j)].name;
                    buttonLine1.Add(buttons[i * fieldWidth + j]);
                    buttonLine2.Add(buttons[i * fieldWidth + (fieldWidth - 1 - j)]);
                }
            }
            test1 += "\n";
            test2 += "\n";
            if (buttonLine1.Count >= chainLengthToWin) {
                buttonLines.Add(buttonLine1);
            }
            if (buttonLine2.Count >= chainLengthToWin) {
                buttonLines.Add(buttonLine2);
            }
        }
        // Debug.Log(test1);
        // Debug.Log(test2);

        // Vertical
        for (int i = 0; i < fieldWidth; i++) {
            List<Button> buttonLine3 = new List<Button>();
            for (int j = 0; j < fieldHeight; j++) {
                test3 += buttons[i + j * fieldWidth].name;
                buttonLine3.Add(buttons[i + j * fieldWidth]);
            }
            test3 += "\n";
            if (buttonLine3.Count >= chainLengthToWin) {
                buttonLines.Add(buttonLine3);
            }
        }
        // Debug.Log(test3);

        // Horizontal
        for (int j = 0; j < fieldHeight; j++) {
            List<Button> buttonLine4 = new List<Button>();
            for (int i = 0; i < fieldWidth; i++) {
                test4 += buttons[i + j * fieldWidth].name;
                buttonLine4.Add(buttons[i + j * fieldWidth]);
            }
            test4 += "\n";
            if (buttonLine4.Count >= chainLengthToWin) { 
                buttonLines.Add(buttonLine4);
            }
        }
        // Debug.Log(test4);

        fieldsLeft = buttons.Count;
        AdaptMaxDepthDynamically();
    }

    private void EnableButtons(bool enabled, bool ignoreEmpty = false) {
        foreach (Button button in buttons) {
            // Do not reanable buttons that already were used
            if (!enabled || ignoreEmpty || IsFieldEmpty(button)) {
                button.interactable = enabled;
            }
        }
    }

    private bool IsFieldEmpty(Button button) {
        return GetText(button).text == "";
    }

    private void SetFontSize(Button button, int fontSize) {
        button.GetComponentInChildren<Text>().fontSize = fontSize;
    }

    private Text GetText(Button button) {
        return button.GetComponentInChildren<Text>();
    }

    private bool SetMarkAndCheckForWin(Button button, bool colorate = false, bool finalChoice = false) {
        Text text = GetText(button);
        if (text.text != "") {
            return false;
        }
        text.text = turn ? "X" : "O";
        Color color = (visualizeAI || finalChoice) ? Color.white : Color.black;
        text.color = color;
        fieldsLeft--;

        return CheckForWin(text.text, colorate);
    }

    public void OnButtonClick(Button button) {
        if (isGameOver) {
            Reset();
            return;
        }
        if (fieldsLeft <= 0) {
            return;
        }

        if (SetMarkAndCheckForWin(button, true, true)) {
            Win(); // Display the game results
        }
        button.interactable = false;
        AdaptMaxDepthDynamically();

        // Game Over - Draw
        if (fieldsLeft <= 0) {
            GameOverDraw();
        }

        // Switch turns
        turn = !turn;

        // Let the AI play
        if (!isGameOver && fieldsLeft > 0 && IsAiTurn()) {
            StartCoroutine(AiTurnCoroutine());
        }
    }

    private bool IsAiTurn() {
        return (turn && p1Ai) || (!turn && p2Ai);
    }

    private int totalStepsNeeded;
    private int stepsPruned;
    private IEnumerator AiTurnCoroutine() {
        EnableButtons(false);
        // Call the MinMax algorithm. It will store the (for the player) worst move in optimalScoreButtonIndex.
        // What is worst for the player, is the best for the AI.
        IEnumerator minMaxEnumerator = MinMaxCoroutine(1);
        GameObject loadingSpinner = aiLoadingSpinners[turn ? 0 : 1];
        loadingSpinner.SetActive(true);
        totalStepsNeeded = 0;
        stepsPruned = 0;
        optimalScoreButtonIndex = 0;
        if (visualizeAI) {
            // Take breaks between all steps so we can see it
            yield return StartCoroutine(minMaxEnumerator);
        } else {
            // Force the coroutine to do many steps in one frame
            while (minMaxEnumerator.MoveNext()) {
                yield return 0;
            }
        }
        Debug.Log(totalStepsNeeded + " steps needed, pruned: " + stepsPruned);
        loadingSpinner.SetActive(false);

        HideDepthAndScoreForAllButtons();

        // Debug.Log("buttonIndex: " + optimalScoreButtonIndex);
        Button button = buttons[optimalScoreButtonIndex]; // Could be random by using: (int)Mathf.Round(Random.Range(0, 9));
        EnableButtons(true);
        OnButtonClick(button);
        AdaptMaxDepthDynamically();
    }

    /// <summary>
    /// Min Max algorithm to find the best and worse moves.
    /// This Method stores the current best and worst moves in
    /// highestCurrentScoreIndex and lowestCurrentScoreIndex as a side effect.
    /// </summary>
    /// <param name="depth">Depth - the number of recursion step for weighting the scores</param>
    /// <returns>The sum of scores of all possible steps from the current recursion level downwards (stored in recursionScore)</returns>
    private IEnumerator MinMaxCoroutine(int depth, int alpha = Int32.MinValue, int beta = Int32.MaxValue, bool isMaximizingPlayer = true) {
        totalStepsNeeded++;
        // Base case and shortcuts (hard coded moves) to stop recursion
        if (CheckBaseCaseAndShortcuts() || depth > maxExploreDepth) {
            yield break;
        }

        // We want to store which field gives us the best (player) or the worst (CPU) score
        int currentBestScore = isMaximizingPlayer ? Int32.MinValue : Int32.MaxValue;
        int currentOptimalScoreButtonIndex = -1;

        // Find next free field
        int fieldIndex = 0;
        while (fieldIndex < buttons.Count) {
            if (IsFieldFree(fieldIndex)) {
                Button button = buttons[fieldIndex];
                int currentScore = 0;
                bool endRecursion = false;

                // Some delay to make it possible to see single steps
                if (visualizeAI && algorithmStepDuration > 0) {
                    yield return new WaitForSeconds(algorithmStepDuration);
                    SetDepth(button, depth);
                } else {
                    yield return 0;

                }

                // End iteration and recursion level when we win, because we don't need to go deeper
                if (SetMarkAndCheckForWin(button)) {
                    currentScore = (isMaximizingPlayer ? 1 : -1) * (buttons.Count + 1 - depth);
                    endRecursion = true;
                } else if (fieldsLeft > 0) {
                    // If there are fields left after the SetMarkAndCheckForWin we can go deeper in the recursion
                    turn = !turn; // Switch turns - in the next step we want to simulate the other player

                    IEnumerator minMaxEnumerator = MinMaxCoroutine(depth + 1, alpha, beta, !isMaximizingPlayer);
                    if (visualizeAI) {
                        // Take breaks between all steps so we can see it
                        yield return StartCoroutine(minMaxEnumerator);
                    } else {
                        // Force the coroutine to do everything in one frame
                        while (minMaxEnumerator.MoveNext()) {
                            if (totalStepsNeeded % endFrameAfterSteps == 0) {
                                Debug.Log("pause after " + totalStepsNeeded);
                                yield return 0;
                            }
                        }
                    }
                    currentScore = recursionScore;
                    turn = !turn; // Switch turns back
                }

                if (isMaximizingPlayer) {
                    if (currentScore > currentBestScore) {
                        currentBestScore = currentScore;
                        currentOptimalScoreButtonIndex = fieldIndex;

                        if (useAlphaBetaPruning) {
                            if (currentBestScore > beta) {
                                endRecursion = true;
                                stepsPruned++;
                            } else {
                                alpha = Math.Max(alpha, currentBestScore);
                            }
                        }
                    }
                } else {
                    if (currentScore < currentBestScore) {
                        currentBestScore = currentScore;
                        currentOptimalScoreButtonIndex = fieldIndex;
                        
                        if (useAlphaBetaPruning) {
                            if (currentBestScore < alpha) {
                                endRecursion = true;
                                stepsPruned++;
                            } else {
                                beta = Math.Min(beta, currentBestScore);
                            }
                        }
                    }
                }

                if (visualizeAI) {
                    SetScore(button, currentScore);
                }

                // Some delay to make it possible to see single steps
                if (visualizeAI && algorithmStepDuration > 0) {
                    yield return new WaitForSeconds(algorithmStepDuration);
                }

                // Undo this step and go to the next field
                GetText(button).text = "";
                if (visualizeAI) {
                    HideDepthAndScore(button);
                }
                fieldsLeft++;

                if (endRecursion) {
                    // No need to check further fields if there already is a win
                    break;
                }
            }
            fieldIndex++;
            // Stop if we checked all buttons
        }

        recursionScore = currentBestScore;
        if (currentOptimalScoreButtonIndex != -1) {
            optimalScoreButtonIndex = currentOptimalScoreButtonIndex;
        }
    }

    private void SetScore(Button button, int score) {
        Text scoreText = button.transform.GetChild(2).GetComponent<Text>();
        scoreText.gameObject.SetActive(true);
        scoreText.text = score + " Points";
    }

    private void SetDepth(Button button, int depth) {
        Text depthText = button.transform.GetChild(1).GetComponent<Text>();
        depthText.gameObject.SetActive(true);
        depthText.text = "Depth: " + depth;

        GetText(button).color = Color.gray;
    }

    private void HideDepthAndScore(Button button) {
        button.transform.GetChild(1).gameObject.SetActive(false);
        button.transform.GetChild(2).gameObject.SetActive(false);
        GetText(button).color = Color.white;
    }

    private void HideDepthAndScoreForAllButtons() {
        foreach (Button button in buttons) {
            HideDepthAndScore(button);
        }
    }

    // This will return true if we can stop the recursion immediately and handle shortcuts (hard coded moves for faster progress)
    private bool CheckBaseCaseAndShortcuts() {
        if (fieldsLeft <= 0) {
            recursionScore = 0;
            return true;
        }

        if (!useShortcuts) {
            return false;
        }

        // No need to calculate anything if all fields are free - any corner is the best.
        // But let's use that chance for some variety and use random. Index will be in the corner (for a 3x3 field eg.: 0, 2, 6 or 8)
        if (fieldsLeft == buttons.Count) {
            RandomCorner();
            return true;
        }
        // Shortcut for the optimal second move after an opening
        if (fieldsLeft == buttons.Count - 1) {
            int centerButtonIndex = (int)Mathf.Floor(buttons.Count / 2);
            // If the other player used the middle. go for any corner
            if (!GetText(buttons[centerButtonIndex]).text.Equals("")) {
                RandomCorner();
            } else { // Else the middle is always the best
                optimalScoreButtonIndex = centerButtonIndex;
            }
            return true;
        }
        return false;
    }

    // Returns true if the given mark if present with three in a row
    private bool CheckForWin(string mark, bool colorate = false) {
        if (fieldsLeft > buttons.Count - chainLengthToWin) {
            return false;
        }
        /*string test = "| ";
        buttonLines.ForEach((line) => {
            line.ForEach((button) => test += button.name + ", ");
            test += " | ";
        });
        Debug.Log(test);*/
        return buttonLines.Any((line) => CompareButtons(line, mark, colorate));

        /*List<Button> diagonalButtons1 = new List<Button>();
        List<Button> diagonalButtons2 = new List<Button>();
        for (int buttonRowIndex = 0; buttonRowIndex < buttonsPerLine; buttonRowIndex++) {
            // Diagonales
            diagonalButtons1.Add(buttons[buttonRowIndex * (buttonsPerLine + 1)]);
            diagonalButtons2.Add(buttons[buttons.Count - buttonsPerLine - buttonRowIndex * (buttonsPerLine - 1)]);

            // Row
            int buttonIndex = buttonRowIndex * buttonsPerLine;
            foundAny |= CompareButtons(buttons.GetRange(buttonIndex, buttonsPerLine), mark, colorate);

            // Column
            List<Button> columnButtons = new List<Button>();
            for (int buttonColumnIndex = 0; buttonColumnIndex < buttonsPerLine; buttonColumnIndex++) {
                columnButtons.Add(buttons[buttonRowIndex + buttonColumnIndex * buttonsPerLine]);
            }
            foundAny |= CompareButtons(columnButtons, mark, colorate);
        }
        foundAny |= CompareButtons(diagonalButtons1, mark, colorate);
        foundAny |= CompareButtons(diagonalButtons2, mark, colorate);

        return foundAny;*/
    }

    private bool CompareButtons(List<Button> buttonRange, string mark, bool colorate = false) {
        List<Text> texts = buttonRange.Select(button => GetText(button)).ToList();
        List<Text> matches = new List<Text>();
        bool equal = false; // texts.All(t => t.text.Equals(mark));

        // Search a chain that is long enough in this line
        foreach (Text t in texts) {
            if (t.text.Equals(mark)) {
                matches.Add(t);

                if (matches.Count >= chainLengthToWin) {
                    equal = true;
                    break;
                }
            } else {
                matches.Clear();
            }
        }

        if (colorate && equal) {
            Color color = turn ? Color.green : Color.red;
            matches.ForEach(t => t.color = color);
        }
        return equal;
    }

    // Checks if a field is still free
    private bool IsFieldFree(int index) => GetText(buttons[index]).text.Length == 0;

    // Displays the game results
    private void Win() {
        // Debug.Log(turn ? "Player 1 won!" : "Game Over!");
        isGameOver = true;
        EnableButtons(false);
        onGameOverDelegate?.Invoke(turn ? 0 : 1);
    }
    private void GameOverDraw() {
        // Debug.Log("Game Over - Draw");
        isGameOver = true;
        EnableButtons(false);
        onGameOverDelegate?.Invoke(-1);
    }

    // Use some variety and use random to determine an optimal start field. Index will be the one of a corner
    private int RandomCorner() {
        optimalScoreButtonIndex = (int)Mathf.Floor(UnityEngine.Random.Range(0, 4));
        if (optimalScoreButtonIndex == 1) {
            optimalScoreButtonIndex = buttons.Count - fieldWidth;
        } else if (optimalScoreButtonIndex == 2) {
            optimalScoreButtonIndex = fieldWidth - 1;
        } else if (optimalScoreButtonIndex == 3) {
            optimalScoreButtonIndex = buttons.Count - 1;
        }
        return optimalScoreButtonIndex;
    }


    // Right click on the script in the inspector to use these methods

    [ContextMenu("Reset")]
    private void Reset() {
        foreach (Button button in buttons) {
            Text text = GetText(button);
            text.color = Color.white;
            text.text = "";
            button.interactable = true;
        }
        fieldsLeft = buttons.Count;
        AdaptMaxDepthDynamically();
        isGameOver = false;
        if (IsAiTurn()) {
            StartCoroutine(AiTurnCoroutine());
        }
    }

    // Right click on the script in the inspector to use this.
    // This only makes sense on a 3x3 game field.
    [ContextMenu("Set Depth Test Example")]
    private void SetDepthTestExample() {
        turn = true;
        Reset();
        GetText(buttons[1]).text = "X"; buttons[1].interactable = false;
        GetText(buttons[5]).text = "X"; buttons[5].interactable = false;
        GetText(buttons[6]).text = "O"; buttons[6].interactable = false;
        GetText(buttons[7]).text = "O"; buttons[7].interactable = false;
        GetText(buttons[8]).text = "X"; buttons[8].interactable = false;

        turn = false;
        fieldsLeft = 4;
        StartCoroutine(AiTurnCoroutine());
    }
}
