using UnityEngine;

public class Rotation : MonoBehaviour {

    public float rotationSpeed = 180;

    void Update() {
        transform.Rotate(0, 0, Time.deltaTime * rotationSpeed, Space.Self);
    }
}
